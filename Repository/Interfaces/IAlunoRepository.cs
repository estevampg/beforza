﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beforza.Repository.Interfaces
{
    interface IAlunoRepository : IRepositoryBase<AlunoModel>
    {
        AlunoModel GetByEmail(string email);
        IQueryable<AlunoModel> GetAlunoByAcademia(int idAcademia);
        IQueryable<AlunoModel> GetAlunoWithoutAcademia();
        //AlunoModel GetTreinoAluno(string email);
    }
}
