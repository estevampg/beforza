﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beforza.Repository.Interfaces
{
    interface IUsuarioRepository : IRepositoryBase<UsuarioModel>
    {
        new UsuarioModel GetById(string id);
    }
}
