﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beforza.Repository.Interfaces
{
    interface ITreinoRepository : IRepositoryBase<TreinoModel>
    {
        //IEnumerable<TreinoModel> BuscarTreinoAluno(string email);
        TreinoModel GetByIdEager(int id);
        //TreinoModel GetByAlunoId(int id);
        //IEnumerable<TreinoModel> GetByAcademiaId(int idAcademia);
        IEnumerable<TreinoModel> BuscarAtivos();
    }
}
