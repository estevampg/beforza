﻿using beforza.Models;
using beforza.Repository.Implementations;
using beforza.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace beforza.Repository.Implamentations
{
    public class AlunoRepository : RepositoryBase<AlunoModel>, IAlunoRepository
    {
        public override void Add(AlunoModel aluno)
        {
            Db.Alunos.Add(aluno);
            //Db.Entry(aluno.Academia).State = EntityState.Modified;
            Db.Entry(aluno).State = EntityState.Added;
            Db.SaveChanges();
        }

        public AlunoModel GetByEmail(string email)
        {
            return Db.Alunos.Where(x => x.Email == email).Single();
        }

        public IQueryable<AlunoModel> GetAlunoByAcademia(int idAcademia)
        {
            return Db.Alunos.Where(a => a.Ativo).Where(a => a.Academia.AcademiaId == idAcademia);
        }

        public IQueryable<AlunoModel> GetAlunoWithoutAcademia()
        {
            return Db.Alunos.Where(a => a.Ativo).Where(a => a.Academia.AcademiaId == 1);
        }

        //public AlunoModel GetTreinoAluno(int idAluno)
        //{
        //    return Db.Alunos.Where(a => a.AlunoId == idAluno);
        //}
    }
}