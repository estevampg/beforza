﻿using beforza.Models;
using beforza.Repository.Implementations;
using beforza.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace beforza.Repository.Implamentations
{
    public class UsuarioRepository : RepositoryBase<UsuarioModel>, IUsuarioRepository
    {
        public new UsuarioModel GetById(string id)
        {
            return Db.Usuarios.Where(u => u.UsuarioId.ToLower().Trim().Equals(id)).Single();
        }
    }
}