﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Microsoft.AspNet.Identity;
using beforza.Context;
using beforza.Repository.Interfaces;
using beforza.Models;
using beforza.Repository.Implementations;
using beforza.Utils;

namespace beforza.Repository.Implamentations
{
    public class TreinoRepository : RepositoryBase<TreinoModel>, ITreinoRepository
    {
        public override void Update(TreinoModel treino)
        {
            var local = Db.Set<TreinoModel>().Local.FirstOrDefault();

            if (local != null)
            {
                Db.Entry(local).State = EntityState.Detached;
            }
            Db.Entry(treino).State = EntityState.Modified;
            Db.SaveChanges();
        }

        public TreinoModel GetByIdEager(int id)
        {
            return Db.Treinos.Where(t => t.TreinoId == id)
                .Include(x => x.Fases.Select(s => s.Series))
                .Include(x => x.Fases.Select(f => f.Series.Select(r => r.RepeticoesCargas))).Single();            
        }

        //.Select(e => e.Exercicio)

        //public TreinoModel GetByAlunoId(int idAluno)
        //{
        //    return Db.Treinos.Where(x => x.Aluno. (a => a.AlunoId == idAluno)).Single();
        //}

        //public IEnumerable<TreinoModel> GetByAcademiaId(int idAcademia)
        //{
        //    return Db.Treinos.Where(x => x.Alunos.Any(a => a.Academia.AcademiaId == idAcademia));
        //}

        public IEnumerable<TreinoModel> BuscarAtivos()
        {
            int idAcademia = Util.GetAcademiaId();
            return Db.Treinos.Where(u => u.Ativo).Where(u => u.Academia.AcademiaId == idAcademia).OrderBy(u => u.Nome).ToList();
        }
    }
}