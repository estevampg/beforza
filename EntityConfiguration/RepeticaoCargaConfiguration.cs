﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace beforza.EntityConfiguration
{
    public class RepeticaoCargaConfiguration : EntityTypeConfiguration<RepeticaoCargaModel>
    {
        public RepeticaoCargaConfiguration()
        {
            ToTable("RepeticaoCarga");

            HasKey(t => t.RepeticaoCargaId);
            Property(t => t.RepeticaoCargaId).HasColumnName("id");
            Property(t => t.Repeticao).HasColumnName("repeticao").IsRequired();
            Property(t => t.Carga).HasColumnName("carga").IsRequired();
            Property(t => t.SerieId).HasColumnName("SerieId").IsRequired();

            HasRequired(c => c.Serie).WithMany(r => r.RepeticoesCargas).HasForeignKey(c => c.SerieId);
        }
    }
}