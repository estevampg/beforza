﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace beforza.EntityConfiguration
{
    public class AcademiaConfiguration : EntityTypeConfiguration<AcademiaModel>
    {
        public AcademiaConfiguration()
        {
            ToTable("Academia");

            HasKey(a => a.AcademiaId);
            Property(a => a.AcademiaId).HasColumnName("id");
            Property(a => a.Nome).HasColumnName("nome").IsRequired();
            Property(a => a.Cnpj).HasColumnName("cnpj").IsRequired();
            Property(a => a.Email).HasColumnName("email").IsRequired();
            Property(a => a.Ativo).HasColumnName("ativo").IsOptional();


        }
    }
}