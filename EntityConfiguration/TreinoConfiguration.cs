﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace beforza.EntityConfiguration
{
    public class TreinoConfiguration : EntityTypeConfiguration<TreinoModel>
    {
        public TreinoConfiguration()
        {
            ToTable("Treino");

            HasKey(t => t.TreinoId);
            Property(t => t.TreinoId).HasColumnName("id");
            Property(t => t.Nome).HasColumnName("nome");
            Property(t => t.Descricao).HasColumnName("descricao");
            Property(t => t.DataFim).HasColumnName("data_fim");
            Property(t => t.Ativo).HasColumnName("ativo").IsRequired();
            Property(t => t.AcademiaId).HasColumnName("AcademiaId");

            HasRequired(x => x.Academia).WithMany(x => x.Treinos).HasForeignKey(m => m.AcademiaId);
        }
    }
}