﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace beforza.EntityConfiguration
{
    public class FaseConfiguration : EntityTypeConfiguration<FaseModel>
    {
        public FaseConfiguration()
        {
            ToTable("Fase");

            HasKey(c => c.FaseId);
            Property(c => c.FaseId).HasColumnName("id");
            Property(c => c.Nome).HasColumnName("nome");
            Property(c => c.TreinoId).HasColumnName("TreinoId");

            //HasRequired(x => x.Treino).WithMany(x => x.Fases).Map(m => m.MapKey("TreinoId"));
            //HasRequired(c => c.Treino).WithMany().HasForeignKey(c => c.TreinoId);

            HasRequired(c => c.Treino).WithMany(f => f.Fases).HasForeignKey(c => c.TreinoId);
        }
    }
}