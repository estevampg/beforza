﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace beforza.EntityConfiguration
{
    public class ExercicioConfiguration : EntityTypeConfiguration<ExercicioModel>
    {
        public ExercicioConfiguration()
        {
            ToTable("Exercicio");

            HasKey(e => e.ExercicioId);
            Property(e => e.ExercicioId).HasColumnName("id");
            Property(e => e.Nome).HasColumnName("nome").IsRequired(); ;
            Property(e => e.Descricao).HasColumnName("descricao").IsRequired();
            Property(e => e.GrupoMuscular).HasColumnName("grupo_muscular").IsRequired();
            Property(e => e.UrlVideo).HasColumnName("url_video").IsOptional();
            Property(e => e.Ativo).HasColumnName("ativo").IsOptional();
        }
    }
}