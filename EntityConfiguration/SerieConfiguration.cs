﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace beforza.EntityConfiguration
{
    public class SerieConfiguration : EntityTypeConfiguration<SerieModel>
    {
        public SerieConfiguration()
        {
            ToTable("Serie");

            HasKey(c => c.SerieId);
            Property(c => c.SerieId).HasColumnName("id");
            Property(c => c.ExercicioId).HasColumnName("ExercicioId");
            Property(c => c.FaseId).HasColumnName("FaseId");


            HasRequired(c => c.Exercicio).WithMany().HasForeignKey(c => c.ExercicioId);
            HasRequired(c => c.Fase).WithMany(f => f.Series).HasForeignKey(c => c.FaseId);
        }
    }
}