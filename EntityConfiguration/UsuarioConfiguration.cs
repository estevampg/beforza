﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace beforza.EntityConfiguration
{
    public class UsuarioConfiguration : EntityTypeConfiguration<UsuarioModel>
    {
        public UsuarioConfiguration()
        {
            ToTable("AspNetUsers");

            HasKey(a => a.UsuarioId);

            Property(u => u.UsuarioId).HasColumnName("Id");
            Property(u => u.Email).HasColumnName("Email");
            Property(u => u.EmailConfirmed).HasColumnName("EmailConfirmed");
            Property(u => u.Username).HasColumnName("UserName");
            Property(u => u.Ativo).HasColumnName("Active");
            Property(u => u.Obs).HasColumnName("Obs").HasMaxLength(500).IsOptional();
            Property(u => u.AcademiaId).HasColumnName("AcademiaId");

            HasRequired(x => x.Academia).WithMany().HasForeignKey(x => x.AcademiaId);
        }
    }
}