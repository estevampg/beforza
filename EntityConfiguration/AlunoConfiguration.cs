﻿using beforza.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace beforza.EntityConfiguration
{
    public class AlunoConfiguration : EntityTypeConfiguration<AlunoModel>
    {
        public AlunoConfiguration()
        {
            ToTable("Aluno");

            HasKey(a => a.AlunoId);
            Property(a => a.AlunoId).HasColumnName("id");
            Property(a => a.Nome).HasColumnName("nome").IsRequired(); ;
            Property(a => a.Matricula).HasColumnName("matricula").IsOptional();
            Property(a => a.Email).HasColumnName("email").IsRequired();
            Property(a => a.Ativo).HasColumnName("ativo").IsOptional();
            Property(a => a.AcademiaId).HasColumnName("AcademiaId");
            Property(a => a.TreinoId).HasColumnName("TreinoId").IsOptional();

            //HasRequired(a => a.Academia).WithMany().HasForeignKey(c => c.AcademiaId);

            //HasOptional(x => x.Academia).WithMany(x => x.Alunos).Map(m => m.MapKey("AcademiaId"));
            //HasRequired(x => x.Treino).WithRequiredDependent().Map(m => m.MapKey("TreinoId"));

            HasRequired(c => c.Academia).WithMany(a => a.Alunos).HasForeignKey(c => c.AcademiaId);
            HasOptional(c => c.Treino).WithMany().HasForeignKey(c => c.TreinoId);
        }
    }
}