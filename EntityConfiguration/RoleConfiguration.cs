﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration;
using beforza.Models;

namespace beforza.EntityConfiguration
{
    public class RoleConfiguration : EntityTypeConfiguration<RoleModel>
    {
        public RoleConfiguration()
        {
            ToTable("AspNetRoles");

            HasKey(r => r.Id);
            Property(r => r.Id).HasColumnName("Id");
            Property(r => r.Nome).HasColumnName("Name");
            Property(r => r.Descricao).HasColumnName("Descricao");
            Property(r => r.Ativo).HasColumnName("Ativo");
        }
    }
}