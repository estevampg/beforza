﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace beforza.Models
{
    public class ExercicioModel
    {
        [JsonIgnore]
        public int ExercicioId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string UrlVideo { get; set; }
        public string GrupoMuscular { get; set; }
        [JsonIgnore]
        public bool Ativo { get; set; }
    }
}