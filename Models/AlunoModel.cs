﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace beforza.Models
{
    public class AlunoModel
    {
        [JsonIgnore]
        public int AlunoId { get; set; }
        public string Nome { get; set; }
        public string Matricula { get; set; }
        public string Email { get; set; }
        [JsonIgnore]
        [ForeignKey("Academia")]
        public int? AcademiaId { get; set; }
        public virtual AcademiaModel Academia { get; set; }
        [JsonIgnore]
        public bool Ativo { get; set; }
        [JsonIgnore]
        [ForeignKey("Treino")]
        public int? TreinoId { get; set; }
        [JsonIgnore]
        public virtual TreinoModel Treino { get; set; }
    }
}