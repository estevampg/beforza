﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace beforza.Models
{
    public class AcademiaModel
    {
        public int AcademiaId { get; set; }
        public string Nome { get; set; }
        public string Cnpj { get; set; }
        public string Email { get; set; }
        [JsonIgnore]
        public bool Ativo { get; set; }

        public virtual ICollection<AlunoModel> Alunos { get; set; }
        public virtual ICollection<TreinoModel> Treinos { get; set; }

        public AcademiaModel()
        {
            Alunos = new List<AlunoModel>();
            Treinos = new List<TreinoModel>();
        }

    }
}