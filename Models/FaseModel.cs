﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace beforza.Models
{
    public class FaseModel
    {
        [ScriptIgnore]
        [JsonIgnore]
        public int FaseId { get; set; }
        public string Nome { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        [ForeignKey("Treino")]
        public int TreinoId { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        public virtual TreinoModel Treino { get; set; }

        public virtual ICollection<SerieModel> Series { get; set; }

        public FaseModel()
        {
            Series = new List<SerieModel>();
        }
    }
}