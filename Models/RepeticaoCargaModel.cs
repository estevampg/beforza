﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace beforza.Models
{
    public class RepeticaoCargaModel
    {
        [ScriptIgnore]
        [JsonIgnore]
        public int RepeticaoCargaId { get; set; }
        public int Repeticao { get; set; }
        public float Carga { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        [ForeignKey("Serie")]
        public int SerieId { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        public virtual SerieModel Serie { get; set; }
    }
}