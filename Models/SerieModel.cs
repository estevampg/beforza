﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace beforza.Models
{
    public class SerieModel
    {
        [ScriptIgnore]
        [JsonIgnore]
        public int SerieId { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        [ForeignKey("Exercicio")]
        public int ExercicioId { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        public virtual ExercicioModel Exercicio { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        [ForeignKey("Fase")]
        public int FaseId { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        public virtual FaseModel Fase { get; set; }

        public virtual ICollection<RepeticaoCargaModel> RepeticoesCargas { get; set; }

        public SerieModel()
        {
            RepeticoesCargas = new List<RepeticaoCargaModel>();
        }
    }
}