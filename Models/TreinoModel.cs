﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace beforza.Models
{
    public class TreinoModel
    {
        [ScriptIgnore]
        [JsonIgnore]
        public int TreinoId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public DateTime DataFim { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        [ForeignKey("Academia")]
        public int AcademiaId { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        public virtual AcademiaModel Academia { get; set; }
        [ScriptIgnore]
        [JsonIgnore]
        public bool Ativo { get; set; }
        public virtual ICollection<FaseModel> Fases { get; set; }

        public TreinoModel()
        {
            Fases = new List<FaseModel>();
        }
    }
}