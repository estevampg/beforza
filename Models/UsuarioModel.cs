﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace beforza.Models
{
    public class UsuarioModel
    {
        [Key]
        public string UsuarioId { get; set; }
        public int AcademiaId { get; set; }
        [DisplayName(@"Empresa")]
        public virtual AcademiaModel Academia { get; set; }
        [DisplayName(@"Usuário")]
        public string Username { get; set; }
        [DisplayName(@"E-mail")]
        public string Email { get; set; }
        [DisplayName(@"E-mail Confirmado")]
        public bool EmailConfirmed { get; set; }
        [JsonIgnore]
        [DisplayName(@"Ativo?")]
        public bool Ativo { get; set; }
        [DataType(DataType.MultilineText)]
        [DisplayName(@"Observações")]
        public string Obs { get; set; }
        //public virtual RoleModel Role { get; set; }
        //public virtual ICollection<TreinoModel> Treinos { get; set; }
        //public string PasswordHash { get; set; }
        //public string SecurityStamp { get; set; }
        //public string PhoneNumber { get; set; }
        //public bool PhoneNumberConfirmed { get; set; }
        //public bool TwoFactorEnabled { get; set; }
        //public DateTime LockoutEndDateUtc { get; set; }
        //public bool LockoutEnabled { get; set; }
        //public int AccessFailedCount { get; set; }
        [NotMapped]
        public string Password { get; set; }
    }
}