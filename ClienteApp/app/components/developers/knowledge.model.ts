﻿export class Knowledge {
    public ionic: number;
    public android: number;
    public ios: number;
    public html: number;
    public css: number;
    public bootstrap: number;
    public jquery: number;
    public angular: number;
    public java: number;
    public aspNet: number;
    public c: number;
    public cPlusPlus: number;
    public cake: number;
    public django: number;
    public majento: number;
    public php: number;
    public wordpress: number;
    public phyton: number;
    public ruby: number;
    public sqlServer: number;
    public mySql: number;
    public salesforce: number;
    public photoshop: number;
    public illustrator: number;
    public seo: number;

    constructor(ionic: number,
            android: number,
            ios: number,
            html: number,
            css: number,
            bootstrap: number,
            jquery: number,
            angular: number,
            java: number,
            aspNet: number,
            c: number,
            cPlusPlus: number,
            cake: number,
            django: number,
            majento: number,
            php: number,
            wordpress: number,
            phyton: number,
            ruby: number,
            sqlServer: number,
            mySql: number,
            salesforce: number,
            photoshop: number,
            illustrator: number,
            seo: number) {
        this.ionic = ionic;
        this.android = android;
        this.ios = ios;
        this.html = html;
        this.css = css;
        this.bootstrap = bootstrap;
        this.jquery = jquery;
        this.angular = angular;
        this.java = java;
        this.aspNet = aspNet;
        this.c = c;
        this.cPlusPlus = cPlusPlus;
        this.cake = cake;
        this.django = django;
        this.majento = majento;
        this.php = php;
        this.wordpress = wordpress;
        this.phyton = phyton;
        this.ruby = ruby;
        this.sqlServer = sqlServer;
        this.mySql = mySql;
        this.salesforce = salesforce;
        this.photoshop = photoshop;
        this.illustrator = illustrator;
        this.seo = seo;
    }

}