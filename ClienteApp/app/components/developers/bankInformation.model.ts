﻿export class BankInformation {
    public cpf: string;
    public bank: string;
    public agency: string;
    public accountType: string;
    public accountNumber: string;

    constructor(cpf: string, bank: string, agency: string, accountType: string,  accountNumber: string) {
        this.cpf = cpf;
        this.bank = bank;
        this.agency = agency;
        this.accountType = accountType;
        this.accountNumber = accountNumber;
    }
}