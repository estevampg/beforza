import { Component, OnInit } from '@angular/core';

import { Developer } from './developer.model';
import { DeveloperService } from './developer.service';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.css']
})
export class DevelopersComponent implements OnInit {
  selectedDeveloper: Developer;

  constructor(private developerService: DeveloperService) { }

  ngOnInit() {
    this.developerService.developerSelected
      .subscribe(
        (developer: Developer) => {
          this.selectedDeveloper = developer;
        }
      );
  }
}
