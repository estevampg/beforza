﻿export class TimeToWork {
    public morning: boolean;
    public afternoon: boolean;
    public night: boolean;
    public dawn: boolean;
    public business: boolean;

    constructor(morning: boolean, afternoon: boolean, night: boolean, dawn: boolean, business: boolean)
    {
        this.morning = morning;
        this.afternoon = afternoon;
        this.night = night;
        this.dawn = dawn;
        this.business = business;
    }
}