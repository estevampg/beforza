import { Component, OnInit, Input } from '@angular/core';

import { Developer } from '../../developer.model';

@Component({
  selector: 'app-developer-item',
  templateUrl: './developer-item.component.html'
})
export class DeveloperItemComponent implements OnInit {
  @Input() developer: Developer;
  @Input() index: number;

  constructor() { }

  ngOnInit() {

  }
}
