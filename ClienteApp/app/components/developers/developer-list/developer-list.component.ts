import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Developer } from '../developer.model';
import { DeveloperService } from '../developer.service';

@Component({
    selector: 'app-developer-list',
    templateUrl: './developer-list.component.html'
})
export class DeveloperListComponent implements OnInit {
    developers: Developer[];
    index: number;

    constructor(private developerService: DeveloperService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.developerService.developersChange
            .subscribe(           
                (developers: Developer[]) => {
                    this.developerService.developers = developers;
                    this.developers = developers;
                }
            )

        this.developerService.getDevelopers()
            .subscribe(
                (developers: Developer[]) => {
                    this.developers = developers;
                })
    }

    onNewDeveloper() {
        this.router.navigate(['new'], { relativeTo: this.route });
    }
}
