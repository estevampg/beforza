import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Developer } from '../developer.model';
import { DeveloperService } from '../developer.service';

@Component({
  selector: 'app-developer-detail',
  templateUrl: './developer-detail.component.html'
})
export class DeveloperDetailComponent implements OnInit {
  developer: Developer;
  id: number;

  constructor(private developerService: DeveloperService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
            this.id = +params['id'];           
            this.developer = this.developerService.getDeveloper(this.id);
        }
      );
  }

  onEditDeveloper() {
    this.router.navigate(['edit'], {relativeTo: this.route})
  }

  onDeleteDeveloper() {
      this.developerService.deleteDeveloper(this.developer.id)
          .subscribe(
              (response) => console.log(response),
              (error) => console.log(error)
          );
      this.router.navigate(['developers']);
  }
}
