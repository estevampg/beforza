import { EventEmitter, Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject'
import 'rxjs/Rx';

import { Developer } from './developer.model';
import { TimeToWork } from './timeToWork.model';
import { HoursPerDay } from './hoursPerDay.model';
import { Knowledge } from './knowledge.model';
import { BankInformation } from './bankInformation.model';

@Injectable()
export class DeveloperService {

    private url: string = "/api/Developers";
    developersChange = new Subject();
    developerSelected = new EventEmitter<Developer>();
    public developers: Developer[];

    constructor(private http: Http) { this.onInit(); }

    onInit() {
        this.http.get(this.url)
            .map(
            (response: Response) => {
                const data = response.json();
                return data;
            }
            ).subscribe(
                (developers: Developer[]) => this.developers = developers,
                (error) => console.log
            );
    }

    getDevelopers() {
        return this.http.get(this.url)
            .map(
                (response: Response) => {
                    const data = response.json();
                    return data;          
                }
            )
    }

    getDeveloper(index: number) {
        return this.developers[index];
    }

    addDeveloper(developer) {
        let body = JSON.stringify(developer);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, options)
            .map(res => res.json().data)
            .finally(() => this.onChange())
    }

    updateDeveloper(developer) {
        let body = JSON.stringify(developer);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.put(this.getDeveloperUrl(developer.id), body, options)
            .map(res => res.json().data)
            .finally(() => this.onChange())
    }

    deleteDeveloper(id) {
        return this.http.delete(this.getDeveloperUrl(id))
            .map(res => res.json())
            .finally(() => this.onChange())
    }

    onChange() {
        this.getDevelopers()
            .subscribe(
                (developers: Developer[]) => {
                    this.developers = developers
                    this.developersChange.next(developers)
                }
            )         
    }

    private getDeveloperUrl(id) {
        return this.url + "/" + id;
    }
}
