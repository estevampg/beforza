﻿export class HoursPerDay {
    public upToFour: boolean;
    public fourToSix: boolean;
    public sixToEight: boolean;
    public upToEight: boolean;
    public weekend: boolean;

    constructor(upToFour: boolean, fourToSix: boolean, sixToEight: boolean, upToEight: boolean, weekend: boolean) {
        this.upToFour = upToFour;
        this.fourToSix = fourToSix;
        this.sixToEight = sixToEight;
        this.upToEight = upToEight;
        this.weekend = weekend;
    }
}