import { Component, OnInit, ApplicationRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Developer } from '../developer.model';
import { DeveloperService } from '../developer.service';

@Component({
    selector: 'app-developer-form',
    templateUrl: './developer-edit.component.html'
})

export class DeveloperEditComponent implements OnInit {
    id: number;
    editMode = false;
    form: FormGroup;
    knowledge = ['ionic', 'android', 'ios', 'html', 'css', 'bootstrap', 'jquery', 'angular', 'java', 'aspNet', 'c', 'cPlusPlus', 'cake', 'django', 'majento', 'php', 'wordpress', 'phyton', 'ruby', 'sqlServer', 'mySql', 'salesforce', 'photoshop', 'illustrator', 'seo'];
    evaluations = [0, 1, 2, 3, 4, 5];
    developer: Developer;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private developerService: DeveloperService,
                private appRef: ApplicationRef) {
    }

    ngOnInit() {
        this.route.params
            .subscribe(
            (params: Params) => {
                this.id = +params['id'];
                this.editMode = params['id'] != null;
                this.initForm();
            }
            );
    }
    private initForm() {
        let id = '';
        let email = '';
        let name = '';
        let skype = '';
        let phone = '';
        let linkedin = '';
        let city = '';
        let state = '';
        let portifolio = '';
        let salaryRequirements = '';
        let upToFour = false;
        let fourToSix = false;
        let sixToEight = false;
        let upToEight = false;
        let weekend = false;
        let morning = false;
        let afternoon = false;
        let night = false;
        let dawn = false;
        let business = false;
        let cpf = '';
        let bank = '';
        let agency = '';
        let accountType = '';
        let accountNumber = '';
        let Ionic = 0;
        let Android = 0;
        let IOS = 0;
        let HTML = 0;
        let CSS = 0;
        let Bootstrap = 0;
        let Jquery = 0;
        let Angular = 0;
        let Java = 0;
        let AspNet = 0;
        let C = 0;
        let CPlusPlus = 0;
        let Cake = 0;
        let Django = 0;
        let Majento = 0;
        let PHP = 0;
        let Wordpress = 0;
        let Phyton = 0;
        let Ruby = 0;
        let SqlServer = 0;
        let MySql = 0;
        let Salesforce = 0;
        let Photoshop = 0;
        let Illustrator = 0;
        let SEO = 0;

        if (this.editMode) {
            const developer = this.developerService.getDeveloper(this.id);
            console.log(developer);
            id = "" + developer.id;
            email = developer.email;
            name = developer.name;
            skype = developer.skype;
            phone = developer.phone;
            linkedin = developer.linkedin;
            city = developer.city;
            state = developer.state;
            portifolio = developer.portifolio;
            salaryRequirements = developer.salaryRequirements;
            upToFour = developer.hoursPerDay.upToFour;
            fourToSix = developer.hoursPerDay.fourToSix;
            sixToEight = developer.hoursPerDay.sixToEight;
            upToEight = developer.hoursPerDay.upToEight;
            weekend = developer.hoursPerDay.weekend;
            morning = developer.timeToWork.morning;
            afternoon = developer.timeToWork.afternoon;
            night = developer.timeToWork.night;
            dawn = developer.timeToWork.dawn;
            business = developer.timeToWork.business;
            cpf = developer.bankInformation.cpf;
            bank = developer.bankInformation.bank;
            agency = developer.bankInformation.agency;
            accountType = developer.bankInformation.accountType;
            accountNumber = developer.bankInformation.accountNumber;
            Ionic = developer.knowledge.ionic;
            Android = developer.knowledge.android;
            IOS = developer.knowledge.ios;
            HTML = developer.knowledge.html;
            CSS = developer.knowledge.css
            Bootstrap = developer.knowledge.bootstrap;
            Jquery = developer.knowledge.jquery;
            Angular = developer.knowledge.angular;
            Java = developer.knowledge.java;
            AspNet = developer.knowledge.aspNet;
            C = developer.knowledge.c;
            CPlusPlus = developer.knowledge.cPlusPlus;
            Cake = developer.knowledge.cake;
            Django = developer.knowledge.django;
            Majento = developer.knowledge.majento;
            PHP = developer.knowledge.php;
            Wordpress = developer.knowledge.wordpress;
            Phyton = developer.knowledge.phyton;
            Ruby = developer.knowledge.ruby;
            SqlServer = developer.knowledge.sqlServer;
            MySql = developer.knowledge.mySql;
            Salesforce = developer.knowledge.salesforce;
            Photoshop = developer.knowledge.photoshop;
            Illustrator = developer.knowledge.illustrator;
            SEO = developer.knowledge.seo;
        }

        this.form = new FormGroup({   
            'id': new FormControl(id),
            'email': new FormControl(email, Validators.required),
            'name': new FormControl(name, Validators.required),
            'skype': new FormControl(skype, Validators.required),
            'phone': new FormControl(phone, Validators.required),
            'linkedin': new FormControl(linkedin),
            'city': new FormControl(city, Validators.required),
            'state': new FormControl(state, Validators.required),
            'portifolio': new FormControl(portifolio),
            'salaryRequirements': new FormControl(salaryRequirements, Validators.required),
            
            'hoursPerDay': new FormGroup({
                'upToFour': new FormControl(upToFour),
                'fourToSix': new FormControl(fourToSix),
                'sixToEight': new FormControl(sixToEight),
                'upToEight': new FormControl(upToEight),
                'weekend': new FormControl(weekend)
            }),
            'timeToWork': new FormGroup({
                'morning': new FormControl(morning),
                'afternoon': new FormControl(afternoon),
                'night': new FormControl(night),
                'dawn': new FormControl(dawn),
                'business': new FormControl(business)
            }),
            'bankInformation': new FormGroup({
                'cpf': new FormControl(cpf),
                'bank': new FormControl(bank),
                'agency': new FormControl(agency),
                'accountType': new FormControl(accountType),
                'accountNumber': new FormControl(accountNumber),
            }),
            'knowledge': new FormGroup({
                'ionic': new FormControl(Ionic),
                'android': new FormControl(Android),
                'ios': new FormControl(IOS),
                'html': new FormControl(HTML),
                'css': new FormControl(CSS),
                'bootstrap': new FormControl(Bootstrap),
                'jquery': new FormControl(Jquery),
                'angular': new FormControl(Angular),
                'java': new FormControl(Java),
                'aspNet': new FormControl(AspNet),
                'c': new FormControl(C),
                'cPlusPlus': new FormControl(CPlusPlus),
                'cake': new FormControl(Cake),
                'django': new FormControl(Django),
                'majento': new FormControl(Majento),
                'php': new FormControl(PHP),
                'wordpress': new FormControl(Wordpress),
                'phyton': new FormControl(Phyton),
                'ruby': new FormControl(Ruby),
                'sqlServer': new FormControl(SqlServer),
                'mySql': new FormControl(MySql),
                'salesforce': new FormControl(Salesforce),
                'photoshop': new FormControl(Photoshop),
                'illustrator': new FormControl(Illustrator),
                'seo': new FormControl(SEO)
            })        
        });
    }

    onSave() {
        if (this.form.value.id) {
            this.developerService.updateDeveloper(this.form.value)
                .subscribe(
                    (response) => console.log(response),
                    (error) => console.log(error)
                )
        } else {
            this.developerService.addDeveloper(this.form.value)
                .subscribe(
                    (response) => console.log(response),
                    (error) => console.log(error)
                )
        }
        this.router.navigate(['developers']);        
    }
}
