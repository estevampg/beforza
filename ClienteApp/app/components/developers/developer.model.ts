import { TimeToWork } from './timeToWork.model';
import { HoursPerDay } from './hoursPerDay.model';
import { Knowledge } from './knowledge.model';
import { BankInformation } from './bankInformation.model';

export class Developer {
  public id: number;
  public email: string;
  public name: string;
  public skype: string;
  public phone: string;
  public linkedin: string;
  public city: string;
  public state: string;
  public portifolio: string;
  public salaryRequirements: string;
  public timeToWork: TimeToWork;
  public hoursPerDay: HoursPerDay;
  public bankInformation: BankInformation;
  public knowledge: Knowledge;
  public otherLanguageFramework: string;
  public linkCrud: string;

  constructor(id: number, email: string, name: string, skype: string, phone: string, linkedin: string,
                city: string, state: string, portifolio: string, salaryRequirements: string,
                timeToWork: TimeToWork, hoursPerDay: HoursPerDay, bankInformation: BankInformation, knowledge: Knowledge, otherLanguageFrameowrk: string,
                linkCrud: string) {
    this.id = id;
    this.email = email;
    this.name = name;
    this.skype = skype;
    this.phone = phone;
    this.linkedin = linkedin;
    this.city = city;
    this.state = state;
    this.portifolio = portifolio;
    this.salaryRequirements = salaryRequirements;    
    this.timeToWork = timeToWork;
    this.hoursPerDay = hoursPerDay;
    this.bankInformation = bankInformation;
    this.knowledge = knowledge;
    this.otherLanguageFramework = otherLanguageFrameowrk;
    this.linkCrud = linkCrud;
  }
}
