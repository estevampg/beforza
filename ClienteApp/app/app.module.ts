import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './components/app/app.component';
import { DevelopersComponent } from './components/developers/developers.component';
import { DeveloperDetailComponent } from './components/developers/developer-detail/developer-detail.component';
import { DeveloperEditComponent } from './components/developers/developer-edit/developer-edit.component';
import { DeveloperListComponent } from './components/developers/developer-list/developer-list.component';
import { DeveloperService } from './components/developers/developer.service';
import { DeveloperItemComponent } from './components/developers/developer-list/developer-item/developer-item.component';
//import { AppRoutingModule } from './app-routing.module';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';

@NgModule({
    declarations: [
        AppComponent,
        DevelopersComponent,
        DeveloperDetailComponent,
        DeveloperEditComponent,
        DeveloperListComponent,
        DeveloperItemComponent,
        NavBarComponent
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        UniversalModule,
        RouterModule.forRoot([
            { path: '', redirectTo: '/developers', pathMatch: 'full' },
            {
                path: 'developers', component: DevelopersComponent, children: [
                    { path: 'new', component: DeveloperEditComponent },
                    { path: ':id', component: DeveloperDetailComponent },
                    { path: ':id/edit', component: DeveloperEditComponent },
                ]
            },
        ])
    ],
    providers: [DeveloperService],
    bootstrap: [AppComponent]
})
export class AppModule { }
