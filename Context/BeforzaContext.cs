﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using beforza.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration;
using beforza.EntityConfiguration;

namespace beforza.Context
{
    public class BeforzaContext : DbContext
    {
        private string schemaName = "dbo";

        public BeforzaContext()
            : base("BeforzaContext")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<AcademiaModel> Academias { get; set; }
        public DbSet<AlunoModel> Alunos { get; set; }
        public DbSet<ExercicioModel> Exercicios { get; set; }
        public DbSet<SerieModel> Series { get; set; }
        public DbSet<RepeticaoCargaModel> RepeticoesCargas { get; set; }
        public DbSet<TreinoModel> Treinos { get; set; }
        public DbSet<FaseModel> Fases { get; set; }
        //public DbSet<EmailConfirmacaoRegistroModel> EmailsConfirmacao { get; set; }
        //public DbSet<MunicipioModel> Municipios { get; set; }
        public DbSet<RoleModel> Roles { get; set; }
        //public DbSet<UfModel> Ufs { get; set; }
        public DbSet<UsuarioModel> Usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(schemaName);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Properties()
                   .Where(p => p.Name == p.ReflectedType.Name + "Id")
                   .Configure(p => p.IsKey());

            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("varchar"));
            modelBuilder.Properties<string>().Configure(p => p.HasMaxLength(200));
            modelBuilder.Properties<decimal>().Configure(p => p.HasPrecision(18, 2));
            modelBuilder.Properties<Int64>().Configure(p => p.HasColumnType("bigint"));

            modelBuilder.Configurations.Add(new AcademiaConfiguration());
            modelBuilder.Configurations.Add(new AlunoConfiguration());
            modelBuilder.Configurations.Add(new ExercicioConfiguration());
            modelBuilder.Configurations.Add(new SerieConfiguration());
            modelBuilder.Configurations.Add(new RepeticaoCargaConfiguration());
            modelBuilder.Configurations.Add(new TreinoConfiguration());
            modelBuilder.Configurations.Add(new FaseConfiguration());
            modelBuilder.Configurations.Add(new UsuarioConfiguration());
        }

        public override int SaveChanges()
        {

            try
            {
                return base.SaveChanges();
            }
            catch (Exception e)
            {
                throw;
            }
            return base.SaveChanges();
        }
    }
}