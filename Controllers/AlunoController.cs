﻿using beforza.Models;
using beforza.Repository.Implamentations;
using beforza.Repository.Interfaces;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace beforza.Controllers
{
    public class AlunoController : Controller
    {
        private readonly IAlunoRepository _alunoRepository = new AlunoRepository();
        // GET: Aluno
        public ActionResult Index(string searchBy, string search, int? page)
        {
            var alunos = _alunoRepository.GetAll();
            return View(alunos.ToPagedList(page ?? 1, 10));
        }

        // GET: Aluno/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Aluno/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Aluno/Create
        [HttpPost]
        public ActionResult Create(AlunoModel model)
        {
            model.AcademiaId = 1;
            model.Ativo = true;
            _alunoRepository.Add(model);
            return View();
        }

        // GET: Aluno/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Aluno/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Aluno/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Aluno/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
