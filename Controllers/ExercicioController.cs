﻿using beforza.Models;
using beforza.Repository.Implamentations;
using beforza.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace beforza.Controllers
{
    public class ExercicioController : Controller
    {
        private readonly IExercicioRepository _exercicioRepository = new ExercicioRepository();
        // GET: Exercicio
        public ActionResult Index(string searchBy, string search, int? page)
        {
            var exercicios = _exercicioRepository.GetAll();
            return View(exercicios.ToPagedList(page ?? 1, 10));
        }

        // GET: Exercicio/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Exercicio/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Exercicio/Create
        [HttpPost]
        public ActionResult Create(ExercicioModel model)
        {
            model.Ativo = true;
            _exercicioRepository.Add(model);
            return View();
        }

        // GET: Exercicio/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Exercicio/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Exercicio/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Exercicio/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
