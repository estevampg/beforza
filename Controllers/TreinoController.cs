﻿using beforza.Models;
using beforza.Repository.Implamentations;
using beforza.Repository.Interfaces;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace beforza.Controllers
{
    public class TreinoController : Controller
    {
        private readonly ITreinoRepository _treinoRepository = new TreinoRepository();
        private readonly IAlunoRepository _alunoRepository = new AlunoRepository();
        private readonly IAcademiaRepository _academiaRepository = new AcademiaRepository();

        // GET: Treino
        public ActionResult Index(string searchBy, string search, int? page)
        {
            var treinosView = _treinoRepository.GetAll();
            return View(treinosView.ToPagedList(page ?? 1, 10));
        }

        // GET: Treino/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Treino/Create
        [HttpPost]
        public ActionResult Create(TreinoModel treino)
        {
            var academia = _academiaRepository.GetById(1);

            treino.AcademiaId = academia.AcademiaId;

            _treinoRepository.Add(treino);


            return View();
        }

        // GET: Treino/Edit/5
        public ActionResult Edit(int id)
        {
            var treino = _treinoRepository.GetByIdEager(id);
            return View(treino);
        }

        // POST: Treino/Edit/5
        [HttpPost]
        public ActionResult Edit(TreinoModel treino)
        {
            //treino.TreinoId = 1002;
            _treinoRepository.Update(treino);
            return View(treino);
        }

        [HttpGet]
        public ActionResult TreinoToAluno()
        {
            ViewBag.TreinoId = new SelectList(_treinoRepository.GetAll(), "TreinoId", "Nome");
            ViewBag.AlunoId = new SelectList(_alunoRepository.GetAll(), "AlunoId", "Nome");
            return View();
        }

        [HttpPost]
        public ActionResult TreinoToAluno(int TreinoId, int AlunoId)
        {
            ViewBag.TreinoId = new SelectList(_treinoRepository.GetAll(), "TreinoId", "Nome");
            ViewBag.AlunoId = new SelectList(_alunoRepository.GetAll(), "AlunoId", "Nome");

            var aluno = _alunoRepository.GetById(AlunoId);
            aluno.TreinoId = TreinoId;
            _alunoRepository.Update(aluno);
            return View();
        }
    }
}