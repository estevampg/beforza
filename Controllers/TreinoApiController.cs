﻿using beforza.Models;
using beforza.Repository.Implamentations;
using beforza.Repository.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace beforza.Controllers
{
    public class TreinoApiController : ApiController
    {
        private readonly ITreinoRepository _treinoRepository = new TreinoRepository();
        private readonly IAlunoRepository _alunoRepository = new AlunoRepository();

        // POST: api/TreinoApi
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            var zippedBase64Data = request.Content.ReadAsStringAsync().Result;
            var treinoObj = JsonConvert.DeserializeObject<JsonTreinoParametersModel>(zippedBase64Data);
            var aluno = _alunoRepository.GetByEmail(treinoObj.email);

            var treinoCompleto = _treinoRepository.GetByIdEager(aluno.TreinoId.Value);

            return Request.CreateResponse(HttpStatusCode.Accepted, treinoCompleto);
        }
    }
}
