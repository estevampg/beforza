﻿using beforza.Models;
using beforza.Repository.Implamentations;
using beforza.Repository.Interfaces;
using beforza.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace beforza.Controllers
{
    public class AlunoApiController : ApiController
    {
        private readonly IAlunoRepository _alunoRepository = new AlunoRepository();
        private readonly IAcademiaRepository _academiaRepository = new AcademiaRepository();

        // POST api/AlunoApi
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            var zippedBase64Data = request.Content.ReadAsStringAsync().Result;
            var alunoObj = JsonConvert.DeserializeObject<JsonAlunoParametersModel>(zippedBase64Data);
            var alunos = _alunoRepository.GetAll();

            if (!alunos.Where(a => a.Email == alunoObj.email).Any())
            {

                AlunoModel aluno = new AlunoModel();

                aluno.Email = alunoObj.email;
                aluno.Nome = alunoObj.nome;
                aluno.Ativo = true;

                aluno.Academia = _academiaRepository.GetById(1);

                _alunoRepository.Add(aluno);
            }

            return Request.CreateResponse(HttpStatusCode.Created);
        }
    }
}
