# **Summary** #

O beforza é um aplicativo que aproxima academia e personais trainers de seus alunos e clientes, gerenciando de forma simples e prática seus treinos e rotinas.

Através do aplicativo o aluno fica atualizado em cada aspecto de seu treino, se familiarizando com sua rotina e medindo seu desempenho a cada dia.

Por sua vez, academias e personais podem atualizar, quando necessário, os treinos de seus alunos, ter acesso a suas fichas e seus desempenhos, facilitando a interação e comunicação entre ambos.

Através do beforza, traçar e alcançar objetivos físicos fica mais fácil, dinâmico e eficiente!

# **Owners** #

* estevampgarcia@gmail.com (Estevam Garcia)
* mayconjuniocava@gmail.com (Maycon Cava)