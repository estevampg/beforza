﻿using beforza.Models;
using beforza.Repository.Implamentations;
using beforza.Repository.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace beforza.Utils
{ 
    public class Util
    {
        private static readonly ITreinoRepository _treinoRepository = new TreinoRepository();
        private static readonly IAcademiaRepository _academiaRepository = new AcademiaRepository();
        private static readonly IUsuarioRepository _usuarioRepository = new UsuarioRepository();

        public static int GetAcademiaId()
        {
            return GetUsuario().Academia.AcademiaId;
        }

        public static UsuarioModel GetUsuario()
        {
            var usuario = _usuarioRepository.GetById(HttpContext.Current.User.Identity.GetUserId());
            return usuario;
        }
    }
}